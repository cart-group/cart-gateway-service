FROM eclipse-temurin:17-jre-alpine
VOLUME /tmp
RUN mkdir /opt/app
COPY target/*.jar /opt/app/cartgateway.jar
EXPOSE 9995
CMD [ "java", "-jar", "/opt/app/cartgateway.jar"]