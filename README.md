# cart-gateway-service



## Docker Image
Docker Repository:

```
https://hub.docker.com/r/villetorio/cart-gateway-service
```

Docker pull

```
docker pull villetorio/cart-gateway-service
```
## Run via docker compose
```
version: '3.9'
services:
  cart-gateway:
    build: .
    ports:
      - "9995:9995"
    environment:
      grpc.client.cart-service.address: "static://cart:9090"
      
networks: 
  default: 
    external: 
      name: external-bridge 
```

## K8s Deployment/Service
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: cart-gateway
spec:
  selector:
    matchLabels:
      app: cart-gateway
  template:
    metadata:
      labels:
        app: cart-gateway
    spec:
      containers:
      - name: cart-gateway
        image: villetorio/cart-gateway-service:1.0.0
        livenessProbe:
          httpGet:
            path: "/actuator/health/liveness"
            port: 9995
          initialDelaySeconds: 30
          periodSeconds: 10
        readinessProbe:
          httpGet:
            path: "/actuator/health/readiness"
            port: 9995
          initialDelaySeconds: 30
          periodSeconds: 10
        resources:
          limits:
            memory: "512Mi"
            cpu: "500m"
        ports:
        - containerPort: 9995
        env:
          - name: grpc.client.cart-service.address
            value: static://10.96.134.199:9090
---
apiVersion: v1
kind: Service
metadata:
  name: cart-gateway-service
spec:
  type: LoadBalancer
  selector:
    app: cart-gateway
  ports:
  - port: 9995
    targetPort: 9995
```
