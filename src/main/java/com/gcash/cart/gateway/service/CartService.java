package com.gcash.cart.gateway.service;

import java.util.List;
import java.util.UUID;

import com.gcash.cart.gateway.dto.OrderRequest;
import com.gcash.cart.gateway.dto.UpdateOrderRequest;
import com.gcash.cart.gateway.dto.CartOrderResponse;

public interface CartService {
	CartOrderResponse addToCart(List<OrderRequest> orders);
	CartOrderResponse addToCart(UUID cartId, List<OrderRequest> orders);
	CartOrderResponse deleteFromCart(UUID cartId, List<UUID> orderIds);
	CartOrderResponse viewCart(UUID cartId);
	CartOrderResponse updateCart(UUID cartId, List<UpdateOrderRequest> orders);
	CartOrderResponse checkoutCart(UUID cartId);
}
