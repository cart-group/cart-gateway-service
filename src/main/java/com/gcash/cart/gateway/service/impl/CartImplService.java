package com.gcash.cart.gateway.service.impl;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.gcash.cart.gateway.dto.OrderRequest;
import com.gcash.cart.gateway.dto.CartOrderResponse;
import com.gcash.cart.gateway.dto.UpdateOrderRequest;
import com.gcash.cart.gateway.service.CartService;
import com.gcash.cart.grpc.GCartOrderRequest;
import com.gcash.cart.grpc.GCartOrderResponse;
import com.gcash.cart.grpc.GOrderRequest;
import com.gcash.cart.grpc.GUpdateCartRequest;
import com.gcash.cart.grpc.GUpdateOrderRequest;
import com.gcash.cart.grpc.GViewCartRequest;
import com.gcash.cart.grpc.GCartServiceGrpc.GCartServiceBlockingStub;
import com.gcash.cart.grpc.GCheckoutCartRequest;
import com.gcash.cart.grpc.GDeleteFromCartRequest;

import net.devh.boot.grpc.client.inject.GrpcClient;

@Service
public class CartImplService implements CartService {
	
	@GrpcClient("cart-service")
	private GCartServiceBlockingStub gCartServiceBlockingStub;
	
	@Autowired
	private ConversionService conversionService;
	
	@Override
	public CartOrderResponse addToCart(List<OrderRequest> orders) {
		GCartOrderRequest request = convert(null, orders);
		GCartOrderResponse gCartOrderResponse = gCartServiceBlockingStub.addToCart(request);
		return conversionService.convert(gCartOrderResponse, CartOrderResponse.class);
	}

	@Override
	public CartOrderResponse addToCart(UUID cartId, List<OrderRequest> orders) {
		GCartOrderRequest request = convert(cartId, orders);
		GCartOrderResponse gCartOrderResponse = gCartServiceBlockingStub.addToCart(request);
		return conversionService.convert(gCartOrderResponse, CartOrderResponse.class);
	}

	@Override
	public CartOrderResponse deleteFromCart(UUID cartId, List<UUID> orderIds) {
		GDeleteFromCartRequest request = GDeleteFromCartRequest.newBuilder()
				.addAllOrderIds(orderIds.stream().map(UUID::toString).toList())
				.setId(cartId.toString())
				.build();
		GCartOrderResponse gCartOrderResponse = gCartServiceBlockingStub.deleteFromCart(request);
		return conversionService.convert(gCartOrderResponse, CartOrderResponse.class);
	}

	@Override
	public CartOrderResponse viewCart(UUID cartId) {
		GViewCartRequest request = GViewCartRequest.newBuilder()
				.setId(cartId.toString())
				.build();
		GCartOrderResponse gCartOrderResponse = gCartServiceBlockingStub.viewCart(request);
		return conversionService.convert(gCartOrderResponse, CartOrderResponse.class);
	}

	@Override
	public CartOrderResponse updateCart(UUID cartId, List<UpdateOrderRequest> orders) {
		List<GUpdateOrderRequest> requestOrders = orders.stream().map(order -> conversionService.convert(order, GUpdateOrderRequest.class)).toList();
		
		GUpdateCartRequest request = GUpdateCartRequest.newBuilder()
				.setId(cartId.toString())
				.addAllOrders(requestOrders)
				.build();
		
		GCartOrderResponse gCartOrderResponse = gCartServiceBlockingStub.updateCart(request);
		return conversionService.convert(gCartOrderResponse, CartOrderResponse.class);
	}
	
	private GCartOrderRequest convert(UUID cartId, List<OrderRequest> orders) {
		List<GOrderRequest> gOrderRequest = orders.stream()
				.map(order -> conversionService.convert(order, GOrderRequest.class))
				.toList();
		
		GCartOrderRequest request = GCartOrderRequest.newBuilder()
				.addAllOrders(gOrderRequest)
				.buildPartial();
		
		if (!ObjectUtils.isEmpty(cartId)) {
			request = GCartOrderRequest.newBuilder(request)
					.setId(cartId.toString())
					.build();
		}
		
		return request;	
	}

	@Override
	public CartOrderResponse checkoutCart(UUID cartId) {
		GCheckoutCartRequest request = GCheckoutCartRequest.newBuilder()
				.setId(cartId.toString())
				.build();
		GCartOrderResponse gCartOrderResponse = gCartServiceBlockingStub.checkoutCart(request);
		return conversionService.convert(gCartOrderResponse, CartOrderResponse.class);
	}

}
