package com.gcash.cart.gateway.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcash.cart.gateway.dto.OrderRequest;
import com.gcash.cart.gateway.dto.UpdateOrderRequest;
import com.gcash.cart.gateway.dto.CartOrderResponse;
import com.gcash.cart.gateway.service.CartService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("carts")
@Validated
public class CartController {
	@Autowired
	private CartService cartService;
	
	@PostMapping
	public ResponseEntity<CartOrderResponse> createCart(@RequestBody @Valid List<OrderRequest> orders) {
		return ResponseEntity.ok(cartService.addToCart(orders));
	}
	
	@PostMapping("/{cartId}/orders")
	public ResponseEntity<CartOrderResponse> addToCart(@PathVariable("cartId") UUID cartId, 
			@RequestBody @Valid List<OrderRequest> orders) {
		return ResponseEntity.ok(cartService.addToCart(cartId, orders));
	}
	
	@DeleteMapping("/{cartId}/orders")
	public ResponseEntity<Void> deleteFromCart(@PathVariable UUID cartId,
			@RequestBody @Valid List<UUID> orderIds) {
		cartService.deleteFromCart(cartId, orderIds);
		return ResponseEntity.accepted().build();
	}
	
	@GetMapping("/{cartId}")
	public ResponseEntity<CartOrderResponse> getCart(@PathVariable UUID cartId) {
		return ResponseEntity.ok(cartService.viewCart(cartId));
	}
	
	@PutMapping("/{cartId}")
	public ResponseEntity<CartOrderResponse> updateCart(@PathVariable UUID cartId, 
			@RequestBody @Valid List<UpdateOrderRequest> orders) {
		return ResponseEntity.ok(cartService.updateCart(cartId, orders));
	}
	
	@PostMapping("/{cartId}/checkout")
	public ResponseEntity<CartOrderResponse> checkoutCart(@PathVariable UUID cartId) {
		return ResponseEntity.ok(cartService.checkoutCart(cartId));
	}
}
