package com.gcash.cart.gateway.dto;

import java.util.UUID;

import lombok.Data;

@Data
public class ResponseOrder {
	private UUID id;
	
	private UUID productId;
	
	private int quantity;
}
