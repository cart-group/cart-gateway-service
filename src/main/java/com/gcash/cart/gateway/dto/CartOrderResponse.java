package com.gcash.cart.gateway.dto;

import java.util.List;
import java.util.UUID;

import lombok.Data;

@Data
public class CartOrderResponse {
	private UUID cartId;
	
	private String status;
	
	List<ResponseOrder> orders;
}
