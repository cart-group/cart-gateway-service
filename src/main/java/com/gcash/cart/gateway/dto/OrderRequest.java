package com.gcash.cart.gateway.dto;

import java.util.UUID;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class OrderRequest {
	@NotNull(message = "productId must be a valid uuid")
	private UUID productId;
	
	@Positive(message = "quantity must be greater than 0")
	private int quantity;
}
