package com.gcash.cart.gateway.dto;

import java.util.UUID;

import lombok.Data;

@Data
public class UpdateOrderRequest {
	private UUID id;
	
	private int quantity;
}
