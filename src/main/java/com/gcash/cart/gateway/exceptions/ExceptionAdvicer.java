package com.gcash.cart.gateway.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import io.grpc.StatusRuntimeException;
import jakarta.validation.ConstraintViolationException;

@ControllerAdvice
public class ExceptionAdvicer {

	@ExceptionHandler(StatusRuntimeException.class)
	public ResponseEntity<ErrorResponse> handleStatusRuntimeException(StatusRuntimeException e) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setCode("CARTX");
		if (e.getMessage().contains("NOT_FOUND:")) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
		}
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<ErrorResponse> handleConstraintViolationException(ConstraintViolationException e) {
		ErrorResponse response = new ErrorResponse();
		response.setError(e.getMessage());
		response.setCode("CARTVLDE");
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
	}
}
