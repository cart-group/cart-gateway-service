package com.gcash.cart.gateway.exceptions;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_NULL)
public class ErrorResponse {
	private String error;
	
	private List<String> errors;
	
	private String code;
	
	private List<String> codes;
}
