package com.gcash.cart.gateway.converter;

import org.springframework.core.convert.converter.Converter;

import com.gcash.cart.gateway.dto.OrderRequest;
import com.gcash.cart.grpc.GOrderRequest;

public class OrderRequestToGOrderRequestConverter 
	implements Converter<OrderRequest, GOrderRequest>{

	@Override
	public GOrderRequest convert(OrderRequest source) {
		return GOrderRequest.newBuilder()
				.setProductId(source.getProductId().toString())
				.setQuantity(source.getQuantity())
				.build();
	}

}
