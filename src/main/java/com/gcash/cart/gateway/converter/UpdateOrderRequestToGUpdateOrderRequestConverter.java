package com.gcash.cart.gateway.converter;

import org.springframework.core.convert.converter.Converter;

import com.gcash.cart.gateway.dto.UpdateOrderRequest;
import com.gcash.cart.grpc.GUpdateOrderRequest;

public class UpdateOrderRequestToGUpdateOrderRequestConverter 
	implements Converter<UpdateOrderRequest, GUpdateOrderRequest>{

	@Override
	public GUpdateOrderRequest convert(UpdateOrderRequest source) {
		return GUpdateOrderRequest.newBuilder()
				.setId(source.getId().toString())
				.setQuantity(source.getQuantity())
				.build();
	}

}
