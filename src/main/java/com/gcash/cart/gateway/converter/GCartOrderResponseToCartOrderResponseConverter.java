package com.gcash.cart.gateway.converter;

import java.util.List;
import java.util.UUID;

import org.springframework.core.convert.converter.Converter;

import com.gcash.cart.gateway.dto.CartOrderResponse;
import com.gcash.cart.gateway.dto.ResponseOrder;
import com.gcash.cart.grpc.GCartOrderResponse;
import com.gcash.cart.grpc.GOrderResponse;

public class GCartOrderResponseToCartOrderResponseConverter 
	implements Converter<GCartOrderResponse, CartOrderResponse>{

	@Override
	public CartOrderResponse convert(GCartOrderResponse source) {
		List<GOrderResponse> orderResponse = source.getOrdersList();
		List<ResponseOrder> orders = orderResponse.stream().map(order -> {
			ResponseOrder responseOrder = new ResponseOrder();
			responseOrder.setId(UUID.fromString(order.getId()));
			responseOrder.setProductId(UUID.fromString(order.getProductId()));
			responseOrder.setQuantity(order.getQuantity());
			return responseOrder;
		}).toList();
		
		CartOrderResponse cartOrderResponse = new CartOrderResponse();
		cartOrderResponse.setCartId(UUID.fromString(source.getId()));
		cartOrderResponse.setStatus(source.getStatus());
		cartOrderResponse.setOrders(orders);
		
		return cartOrderResponse;
	}

}
