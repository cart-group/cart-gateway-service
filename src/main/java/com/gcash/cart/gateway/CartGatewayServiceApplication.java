package com.gcash.cart.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartGatewayServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartGatewayServiceApplication.class, args);
	}

}
