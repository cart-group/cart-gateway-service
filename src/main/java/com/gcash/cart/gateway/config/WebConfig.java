package com.gcash.cart.gateway.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.gcash.cart.gateway.converter.GCartOrderResponseToCartOrderResponseConverter;
import com.gcash.cart.gateway.converter.OrderRequestToGOrderRequestConverter;
import com.gcash.cart.gateway.converter.UpdateOrderRequestToGUpdateOrderRequestConverter;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addConverter(new GCartOrderResponseToCartOrderResponseConverter());
		registry.addConverter(new OrderRequestToGOrderRequestConverter());
		registry.addConverter(new UpdateOrderRequestToGUpdateOrderRequestConverter());
	}

}
