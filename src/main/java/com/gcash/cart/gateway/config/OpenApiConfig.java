package com.gcash.cart.gateway.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@Configuration
public class OpenApiConfig {
	@Value("${app.title}") 
	private String appTitle;
	@Value("${app.description}") 
	private String appDescription;
	@Value("${app.version}") 
	private String appVersion;
	
	@Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI()
				.info(new Info()
				.title(appTitle)
				.version(appVersion)
				.description(appDescription)
				.termsOfService("http://swagger.io/terms/")
				.license(new License().
                         name("Apache 2.0").
                         url("http://springdoc.org")));
	}
}
